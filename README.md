# Read News

## Quick Start

Get started...

You need to clone the repo, go to folder `/src` and open `/front` and `/back` projects in your favorite code editor.
IF you want to configure your own database, in `src/back/.env` replace MONGODB_URI value.
You can visit [here](https://news-eight-taupe.vercel.app/) to see online how it works. Once you had some fun, please, refresh the data at [api](https://hn.algolia.com/api/v1/search_by_date?query=nodejs)

## Available Scripts

To the backend you need to:

```shell
# install deps
yarn install

#run
yarn dev
```

To the frontend you need to:

```shell
# install deps
yarn install

#run
yarn start
```

---

## Technologies

- [Typescript](https://www.typescriptlang.org/) - Typescript is a typed superset of JavaScript that compiles to plain JavaScript
- [Express.js](https://www.expressjs.com) - Fast, unopinionated, minimalist web framework for Node.js
- [Mongoose](https://github.com/motdotla/dotenv) - Mongoose is a MongoDB object modeling tool designed to work in an asynchronous environment.
- [Pino](https://github.com/pinojs/pino) - Extremely fast node.js logger, inspired by Bunyan. It also includes a shell utility to pretty-print its log files
- [dotenv](https://github.com/motdotla/dotenv) - Loads environment variables from .env for nodejs projects
