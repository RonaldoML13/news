import React from "react";
import "./../../css/header.css";

export const Header = () => {
  return (
    <div className="container">
      <h1>HN Feed</h1>
      <div>We &#128155; hacker news!</div>
    </div>
  );
};
