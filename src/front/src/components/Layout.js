import React, { useContext, useEffect, useState } from "react";
import toast, { Toaster } from "react-hot-toast";
import { UpdateContext } from "../utils/context/UpdateContext";
import { back_URL } from "../utils/env";
import { Header } from "./header/Header";
import { NewsGrid } from "./table/NewsGrid";

export const Layout = () => {
  const [docs, setDocs] = useState(null);

  const { update, setUpdate } = useContext(UpdateContext);

  useEffect(() => {
    const fetchData = async () => {
      if (update) {
        try {
          const resp = await fetch(`${back_URL}/app/v1/news`);
          const results = await resp.json();
          if (results.ok) {
            setDocs(results.docs);
          } else {
            console.log(results.msg);
            toast.error(results.msg);
          }
          setUpdate(false);
        } catch (error) {
          console.log(error);
          toast.error(error);
        }
      }
    };
    return fetchData();
  }, [setUpdate, update]);
  return (
    <div>
      <Toaster />
      <Header />
      {docs && <NewsGrid news={docs} />}
    </div>
  );
};
