import React, { useContext } from "react";
import PropTypes from "prop-types";
import toast, { Toaster } from "react-hot-toast";
import { UpdateContext } from "../../utils/context/UpdateContext";
import { format } from "../../utils/dateFormatter";
import { back_URL } from "../../utils/env";
import "./../../css/news.css";

export const NewsRow = ({ data }) => {
  const title = data.story_title || data.title;
  const url = data.story_url || data.url;
  const author = data.author;
  const date = data.created_at;

  const { setUpdate } = useContext(UpdateContext);

  const handleDelete = async () => {
    const _data = data.story_title
      ? { name: `${title}`, tag: "story_title" }
      : { name: `${title}`, tag: "title" };
    try {
      const resp = await fetch(`${back_URL}/app/v1/news`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(_data),
      });
      const result = await resp.json();
      if (result.ok) {
        console.log(result);
        toast.success(result.msg);
      } else {
        console.log(result);
        toast.error(result.err);
      }
    } catch (error) {
      console.log({ error });
    }
    return setUpdate(true);
  };
  return (
    <div className="row">
      <Toaster />
      <div className="main-container">
        <div
          className="title"
          onClick={() => {
            window.open(url, title);
          }}
        >
          {title}
        </div>
        <div className="author">{author}</div>
      </div>
      <div className="secondary-container">
        <div className="time">{format(date)}</div>
        <button className="trash" onClick={handleDelete}>
          🗑️
        </button>
      </div>
    </div>
  );
};

NewsRow.propTypes = {
  data: PropTypes.object,
};
