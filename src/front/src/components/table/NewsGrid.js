import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import { NewsRow } from "./NewsRow";

export const NewsGrid = ({ news }) => {
  const [filteredNews, setFilteredNews] = useState(null);
  useEffect(() => {
    const filterNew = () => {
      setFilteredNews(
        news.filter(
          (element) => element.show && (element.story_title || element.title)
        )
      );
    };
    return filterNew();
  }, [news]);

  return (
    <div className="grid-container">
      {filteredNews &&
        filteredNews.map((element, index) => (
          <NewsRow key={index} data={element} />
        ))}
    </div>
  );
};

NewsGrid.propTypes = {
  news: PropTypes.array,
};
