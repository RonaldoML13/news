export const format = (date) => {
  const now = new Date();
  const time = new Date(date);
  if (now.getDay() === time.getDay()) {
    return time.toTimeString().split(" ")[0];
  }
  if (now.getDate() - 1 === time.getDate()) {
    return "yesterday";
  }
  return time.toDateString();
};
