import React, { useState } from "react";

import "./App.css";
import { Layout } from "./components/Layout";
import { UpdateContext } from "./utils/context/UpdateContext";

function App() {
  const [update, setUpdate] = useState(true);
  return (
    <UpdateContext.Provider
      value={{
        update,
        setUpdate,
      }}
    >
      <div className="App">
        <Layout />
      </div>
    </UpdateContext.Provider>
  );
}

export default App;
