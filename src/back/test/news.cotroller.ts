import "mocha";
import { expect } from "chai";
import request from "supertest";
import Server from "../server";

describe("News", () => {
    console.log('object')
    it("should get all the news", () =>
      request(Server)
        .get("/api/v1/news")
        .expect("Content-Type", /json/)
        .then((r) => {
          expect(r.body).to.be.an("array");
        }));
})